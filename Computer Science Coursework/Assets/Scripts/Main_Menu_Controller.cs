﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
 using UnityEngine.SceneManagement;

public class Main_Menu_Controller : MonoBehaviour
{
    //get the buttons from the menu
    public Button playBtn;
    public Button settBtn;
    void Start () {
		
        //setup listeners to detects when the player interacts with the menu
		playBtn.onClick.AddListener(playClicked);

        settBtn.onClick.AddListener(settClicked);
	}

    //Switch the scene when needed
    public void playClicked()
    {
        Debug.Log("Play");
        SceneManager.LoadScene("Main_Scene");
    }

    public void settClicked()
    {
        Debug.Log("Settings");
        SceneManager.LoadScene("Settings");
    }
}
