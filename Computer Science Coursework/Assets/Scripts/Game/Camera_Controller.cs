﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Controller : MonoBehaviour
{
    //variables used to set the offset of the camera from the player
    public float height; 
    public float distance;


    //variables used to get the players position so that the camera position can be relative to it
    public GameObject Player;
    private Transform playerTransform;

    // Start is called before the first frame update
    void Start()
    {
        playerTransform = Player.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = playerTransform.position; //get the players position

        pos.y += height; // move up by the height offset
        pos.z -= distance; //move back by the distance offset

        GetComponent<Transform>().position = pos; // sets the camera positon to pos
    }
}
