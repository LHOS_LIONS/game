﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class Settings_Controller : MonoBehaviour
{
    //get the UI elements used to change the settings. These need to be serialized to allow them to be saved to the registry
    [SerializeField]
    public TMP_Dropdown EnemiesSelector;

    [SerializeField]
    public TMP_Dropdown AlgorithmSelector;

    [SerializeField]
    public TMP_Dropdown ResolutionSelector; 

    [SerializeField]   
    public Toggle FullscreenSelector;

    [SerializeField]
    public TMP_Dropdown QualitySelector;

    public Button MenuButton;

    private List<int[]> resolutions = new List<int[]>();
    
    private int full = 1;
    string[] qualityNames;

    void Start()
    {
        updateDropdowns();

        addListeners();
    }

    private void addListeners()
    {
        EnemiesSelector.onValueChanged.AddListener(delegate
        {
            updateSetting();
        });

        AlgorithmSelector.onValueChanged.AddListener(delegate
        {
            updateSetting();
        });

        ResolutionSelector.onValueChanged.AddListener(delegate
        {
            updateSetting();
        });

        QualitySelector.onValueChanged.AddListener(delegate
        {
            updateSetting();
        });

        FullscreenSelector.onValueChanged.AddListener(delegate
        {
            updateSetting();
        });

        MenuButton.onClick.AddListener(toMenu);
    }

    void updateDropdowns()
    {
        EnemiesSelector.value = PlayerPrefs.GetInt("Enemy#");

        AlgorithmSelector.value = PlayerPrefs.GetInt("EnemyAlg");

        QualitySelector.value = PlayerPrefs.GetInt("Quality");

        UpdateQualityDropdown();

        UpdateResolutionDropdown();

        UpdateFullscreenToggle();
    }

    private void UpdateFullscreenToggle()
    {
        full = PlayerPrefs.GetInt("Full", 1);
        if (full == 1)
        {
            FullscreenSelector.isOn = true;
        }
        else
        {
            FullscreenSelector.isOn = false;
        }
    }

    private void UpdateResolutionDropdown()
    {
        ResolutionSelector.value = PlayerPrefs.GetInt("Resolution");

        resolutions = new List<int[]>();

        foreach (TMP_Dropdown.OptionData option in ResolutionSelector.options)
        {
            string[] split = option.text.Split('×');

            int[] splitInt = new int[2] { int.Parse(split[0]), int.Parse(split[1]) };

            resolutions.Add(splitInt);
        }
    }

    private void UpdateQualityDropdown()
    {
        //create a list of the availible quality options in settings

        List<TMPro.TMP_Dropdown.OptionData> dropdownOptions = new List<TMPro.TMP_Dropdown.OptionData>();

        qualityNames = QualitySettings.names;

        foreach (string i in qualityNames)
        {
            dropdownOptions.Add(new TMP_Dropdown.OptionData() { text = i });
        }

        //clear the options in the dropdown, then add the ones from settings
        QualitySelector.options.Clear();

        QualitySelector.AddOptions(dropdownOptions);

        //set the value of the dropdown to the player's preference
        QualitySelector.value = PlayerPrefs.GetInt("Quality");
    }

    void updateSetting()
    {
        PlayerPrefs.SetInt("Enemy#", EnemiesSelector.value);

        PlayerPrefs.SetInt("EnemyAlg", AlgorithmSelector.value);

        PlayerPrefs.SetInt("Resolution", ResolutionSelector.value);

        PlayerPrefs.SetInt("Full", full);

        int[] res = resolutions[ResolutionSelector.value];
        Screen.SetResolution(res[0], res[1], FullscreenSelector.isOn, 60);    


        PlayerPrefs.SetInt("Quality", QualitySelector.value);
        QualitySettings.SetQualityLevel(QualitySelector.value);

        PlayerPrefs.Save();
    }

    void toMenu()
    {
        SceneManager.LoadScene("Main_Menu");
    }
}
