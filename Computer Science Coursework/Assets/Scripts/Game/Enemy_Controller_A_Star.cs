﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Enemy_Controller_A_Star : MonoBehaviour
{
    
    public Tilemap Map; //the tilemap used for pathfinding
    public TileBase walkableArea; //the type of tile that can be walked on

    public float pathFindInterval = 5.0f; //how frequently (in seconds) the path should be recalculated

    public GameObject Target; //the object that the enemy is trying to get to

    private Rigidbody2D rb2d; //the rigidbody2D component of the enemy

    public List<Vector3> path = new List<Vector3>(); //this list contains points to move between to reach the player

    public float rotationSpeed = 3;
    public float movementForce = 40;
    public float maxSpeed = 5;



    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();

        clearPath();

        pathfind();

        showPath();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 selfPos = rb2d.position;

        //if the enemy has reached the next point on the path, recalculate the path
        if (Vector2Int.RoundToInt(selfPos) == Vector2Int.RoundToInt(path[1]))
        {
            clearPath();

            pathfind();

            showPath();
        }
        
        //calculate the direction to travel in
        float targetRotation = calcRotation(rb2d, path[1]);


        //if the enemy is not pointing towards the target, rotate them so that they are
        if (targetRotation != rb2d.rotation)
        {
            if 
            (
                rb2d.rotation < targetRotation &&
                Mathf.Abs(rb2d.rotation - targetRotation) > rotationSpeed
            )
            {
                rb2d.rotation += rotationSpeed;
            }
            else if 
            (
                rb2d.rotation > targetRotation &&
                Mathf.Abs(rb2d.rotation - targetRotation) > rotationSpeed
            )
            {
                rb2d.rotation -= rotationSpeed;
            }
            else
            {
                rb2d.rotation = targetRotation;
            }
        }
        //if the enemy is pointing towards the target, accelerate towards it
        else
        {
            //accelerate the enemy by the desired amount if possible
            if (rb2d.velocity.magnitude < maxSpeed) //if the max speed has not already been reached
            {   
                Vector2 directionVector = calcDirectionVector(); //calculates the ratio of the vertical and horizontal components
                rb2d.AddForce(movementForce * directionVector);//multiplies this by the input in the vertical axis and applies it as a force
            }
        }
    }

    public Tilemap pathfindIndicator;

    public TileBase pathTileIndicator;

    void clearPath()
    {
        foreach (Vector3 i in path)
        {
            pathfindIndicator.SetTile(Vector3Int.RoundToInt(i), null);
        }
    }
    void showPath()
    {
        foreach (Vector3 i in path)
        {
            pathfindIndicator.SetTile(Vector3Int.RoundToInt(i), pathTileIndicator);
        }
    }
    float calcRotation(Rigidbody2D self, Vector3 target)
    {
        Vector3 selfPos = self.position;

        float angleDeg = 0;

        // check along the axis
        if
        (
            selfPos.x == target.x &&
            selfPos.y > target.y
        )
        {
            angleDeg = 0f;
        }
        else if
        (
            selfPos.x == target.x &&
            selfPos.y > target.y
        )
        {
            angleDeg = 180f;
        }
        else if
        (
            selfPos.x < target.x &&
            selfPos.y == target.y
        )
        {
            angleDeg = -90f;
        }
        else if
        (
            selfPos.x > target.x &&
            selfPos.y == target.y
        )
        {
            angleDeg = 90f;
        }

        //check in each of the quadrants

        else if
        (
            selfPos.x > target.x &&
            selfPos.y > target.y
        )
        {
            angleDeg = 90 + Mathf.Abs(Mathf.Rad2Deg * Mathf.Atan((selfPos.y - target.y)/(selfPos.x -target.x)));
        }
        else if
        (
            selfPos.x > target.x &&
            selfPos.y < target.y
        )
        {
            angleDeg =  Mathf.Abs(Mathf.Rad2Deg * Mathf.Atan((selfPos.y - target.y)/(selfPos.x -target.x)));
        }
        else if
        (
            selfPos.x < target.x &&
            selfPos.y > target.y
        )
        {
            angleDeg = -90 - Mathf.Abs(Mathf.Rad2Deg * Mathf.Atan((selfPos.y - target.y)/(selfPos.x -target.x)));
        }
        else if
        (
            selfPos.x > target.x &&
            selfPos.y > target.y
        )
        {
            angleDeg = - Mathf.Abs(Mathf.Rad2Deg * Mathf.Atan((selfPos.y - target.y)/(selfPos.x -target.x)));
        }


        return angleDeg;
    }

    Vector2 calcDirectionVector()
    {
        float h = Mathf.Sin(Mathf.Deg2Rad * - rb2d.rotation); //calculate the horizontal value

        float v = Mathf.Cos(Mathf.Deg2Rad * rb2d.rotation); //calculate the vertical value

        return (new Vector2(h , v));
    }

    void pathfind()
    {
        //get the current position and save it in both float and integer forms
        Vector3 selfPos = GetComponent<Transform>().position;
        Vector3Int selfIntPos = Vector3Int.RoundToInt(selfPos);

        //get the target's current position and save it in both float and integer forms
        Vector3 targetPos = Target.GetComponent<Transform>().position;
        Vector3Int targetIntPos = Vector3Int.RoundToInt(targetPos);


        //creates a dictionary of tiles indexed by their positions
        Dictionary<Vector3Int,tileData> tiles = new Dictionary<Vector3Int, tileData>();

        //add the tile at the current position
        tiles.Add(selfIntPos, new tileData(selfIntPos, targetIntPos, selfIntPos, 0));

        bool foundEnd = false;

        //loop until path found
        do 
        {
            //find the smallest heuristic
            KeyValuePair<Vector3Int,tileData> smallest = tiles.Last();

            foreach (KeyValuePair<Vector3Int,tileData> item in tiles)
            {
                float val = item.Value.getValue();

                if
                (
                    val < smallest.Value.getValue() &&
                    item.Value.isOpen == true
                )
                {
                    smallest = item;
                }
            }
            
            //get all adjacent tiles that are walkable
            List<Vector3Int> adjacent = getAdjacent(smallest, tiles);

            //calculte heuristics for all the adjacent tiles
            foreach (Vector3Int pos in adjacent)
            {
                if (tiles.ContainsKey(pos)) //if the tile is already in tiles,
                {
                    tiles[pos].reCalcPath(smallest.Value.pathLength, smallest.Value.myPos); //recalculate its value
                }
                else //if the tile is not already in tiles
                {
                    tiles.Add(pos, new tileData(pos, smallest.Value.targetPos, smallest.Value.myPos, smallest.Value.pathLength)); //add it to tiles
                }

                //if the end had been found set the loop to stop
                if (tiles[pos].isEnd)
                {
                    foundEnd = true;
                }
            }
        } while (!foundEnd);


        List<Vector3Int> tempPath = new List<Vector3Int>(); //holds the path whilst it is being found

        Vector3Int current = targetIntPos; //moves backwards along the path to record it

        //loops through the path
        bool foundStart = false;
        do
        {
            tempPath.Add(current);

            //set current to the next value
            Vector3Int temp = tiles[current].lastPos;
            current = temp;

            //check if the start has been found
            if (current == selfIntPos)
            {
                foundStart = true;
            }
        } while (!foundStart);

        //reverse the path and save it to the appropriate variable
        tempPath.Reverse();

        List<Vector3> pathConversion = new List<Vector3>();

        pathConversion.Add(selfIntPos);

        foreach (Vector3 element in tempPath)
        {
            pathConversion.Add(element);
        }

        path = pathConversion;
    }

    List<Vector3Int> getAdjacent(KeyValuePair<Vector3Int, tileData> tile, Dictionary<Vector3Int, tileData> tiles)
    {

        //split the input for tile into its components
        Vector3Int position = tile.Key;
        tileData info = tile.Value;

        Vector3Int tilePos = info.myPos;

        List<Vector3Int> adjacentSquares = new List<Vector3Int>();


        //defines the tiles around the starting tile to check    
        List<Vector3Int>nextTiles = new List<Vector3Int>
        {
            new Vector3Int(1,0,0),
            new Vector3Int(0,1,0),
            new Vector3Int(-1,0,0),
            new Vector3Int(0,-1,0)
        };

        Vector3Int checkingTile = new Vector3Int();

        //check each of the adjacent tiles
        foreach (Vector3Int i in nextTiles)
        {
            checkingTile = tilePos + i;

            if (Map.GetTile(checkingTile) == walkableArea)
            {
                adjacentSquares.Add(checkingTile);
            }
        }

        //check each of the adjacent corners
        checkCorners(tilePos, adjacentSquares);

        info.isOpen = false;

        return adjacentSquares;
    }

    void checkCorners(Vector3Int tilePos, List<Vector3Int> adjacentSquares)
    {
        //checks the upper right corner
        if
        (
            Map.GetTile(tilePos + Vector3Int.up) == walkableArea &&
            Map.GetTile(tilePos + Vector3Int.right) == walkableArea &&
            Map.GetTile(tilePos + Vector3Int.up + Vector3Int.right) == walkableArea
        ){
            adjacentSquares.Add(tilePos + Vector3Int.up + Vector3Int.right);
        }

        //checks the upper left corner
        if
        (
            Map.GetTile(tilePos + Vector3Int.up) == walkableArea&&
            Map.GetTile(tilePos + Vector3Int.left) == walkableArea &&
            Map.GetTile(tilePos + Vector3Int.up + Vector3Int.left) == walkableArea
        ){
            adjacentSquares.Add(tilePos + Vector3Int.up + Vector3Int.left);
        }

        //checks the bottom right corner

        if
        (
            Map.GetTile(tilePos + Vector3Int.down)== walkableArea &&
            Map.GetTile(tilePos + Vector3Int.right)== walkableArea &&
            Map.GetTile(tilePos + Vector3Int.down + Vector3Int.right) == walkableArea
        ){
            adjacentSquares.Add(tilePos + Vector3Int.down + Vector3Int.right);
        }

        //checks the bottom left corner

        if
        (
            Map.GetTile(tilePos + Vector3Int.down) == walkableArea&&
            Map.GetTile(tilePos + Vector3Int.left) == walkableArea&&
            Map.GetTile(tilePos + Vector3Int.down + Vector3Int.left) == walkableArea
        ){
            adjacentSquares.Add(tilePos + Vector3Int.down + Vector3Int.left);
        }
    }
}

public class tileData
{
    public Vector3Int myPos; // the position of this tile
    public Vector3Int targetPos; // the position of the tile that a path is being calculated to
    public Vector3Int lastPos; // the last tile in the path


    public float pathLength; // the lenth of the path to get to this tile
    private float targetDistance; // the distance in a straight line from this tile to the target

    private float tileValue; //the heuristic value for this tile. sum of pathLength and targetDistance


    public bool isOpen = true; // whether this tile still open to be checked
    public bool isEnd = false; // whether this tile is the last tile

    public tileData(Vector3Int _myPos, Vector3Int _targetPos, Vector3Int _lastPos, float lastPathLength)
    {
        //save the parameters to the relevant variables
        myPos = _myPos;
        targetPos = _targetPos;
        lastPos =_lastPos;     



        pathLength = lastPathLength + // the pathlength of the current tile = the last passlength +
            Mathf.Sqrt                // the square root of
            (
                Mathf.Pow
                (
                    Mathf.Abs(myPos.x - lastPos.x), //the difference between the two x positions
                    2                               //squared
                )
                +                                   //plus
                Mathf.Pow
                (
                    Mathf.Abs(myPos.y - lastPos.y), //the difference between the two y postions
                    2                               //squared
                )
            );

        targetDistance = Mathf.Sqrt //the target distance = the square root of
        (
            Mathf.Pow
            (
                Mathf.Abs(myPos.x - targetPos.x), //the difference between the two x positions
                2                                 //squared
            )
            +                                     //plus
            Mathf.Pow
            (
                Mathf.Abs(myPos.y - targetPos.y), //the difference between the two y postions
                2                                 //squared
            )
        );

        tileValue = pathLength + targetDistance; // calculates the heuristic


        //checks if the tile is the end
        if (targetPos == myPos)
        {
            isEnd = true;
        }
    }

    public void reCalcPath(float newPathLength, Vector3Int newLastPos)
    {

        //calculate how long the path would be
        float newPath = newPathLength + Mathf.Sqrt(Mathf.Pow(Mathf.Abs(myPos.x - targetPos.x), 2) + Mathf.Pow(Mathf.Abs(myPos.y - targetPos.y), 2));

        if (newPath < pathLength) //checks the new path against the old path
        {
            //if the new path is quicker, switch to it
            pathLength = newPath;
            tileValue = pathLength + targetDistance;

            lastPos = newLastPos;
            
            isOpen = true;
        }
    }

    public float getValue()
    {
        return tileValue;
    }

}

