﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Game_Over_Controller : MonoBehaviour
{
    //declare variables
    public Button menuButton;
    public Button replayButton;

    public GameObject timeTextObject;
    public GameObject highScoreTextObject;


    // Start is called before the first frame update
    void Start()
    {
        //add listeners to the buttons to enable teh user to inteact with them
        menuButton.onClick.AddListener(loadMenu);
        replayButton.onClick.AddListener(replay);

        //gets the text component of the time text
        TextMeshProUGUI timeText = timeTextObject.GetComponent<TextMeshProUGUI>();
        //set the time component with time rounded to 2 decimal places
        timeText.text = "You survived for: " + Math.Round(Global_Stats.time, 2).ToString() + " seconds";

        //get the high score from the registry
        float highScore = PlayerPrefs.GetFloat("HighScore");

        //get the text component of the highscore text
        TextMeshProUGUI highScoreText = highScoreTextObject.GetComponent<TextMeshProUGUI>();

        //if the user failed to beat the highscore
        if (Global_Stats.time < highScore)
        {
            //set the highscore text to tell the user how far away they were from the highscore
            highScoreText.text = 
                    "You were " +
                    Math.Round(highScore - Global_Stats.time, 2).ToString() +
                    " seconds away from the highscore of " +
                    Math.Round(highScore, 2).ToString();
        }
        else
        {
            highScoreText.text = "You have set the highscore!";
        }
    }

    void loadMenu()
    {
        SceneManager.LoadScene("Main_Menu");
    }

    void replay()
    {
        SceneManager.LoadScene("Main_Scene");
    }
}
