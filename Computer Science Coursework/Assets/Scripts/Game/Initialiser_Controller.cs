﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Initialiser_Controller : MonoBehaviour
{
    public GameObject PlayerPrefab;
    public GameObject EnemyAStarPrefab;
    public GameObject Camera;
    public Tilemap Floor;
    public Tilemap PathfindIndicator;

    public GameObject PlayerSpawn;
    public GameObject[] EnemySpawns;

    // Start is called before the first frame update
    void Start()
    {
        //get position of the spawn point from the editor
        Vector3 playerStartPos = PlayerSpawn.GetComponent<Transform>().position;

        //gets the rotation of the playerPrefab and stores it in an array
        float[] playerRotation = new float[]
        {
            PlayerPrefab.GetComponent<Transform>().rotation.x,
            PlayerPrefab.GetComponent<Transform>().rotation.y,
            PlayerPrefab.GetComponent<Transform>().rotation.z,
            PlayerPrefab.GetComponent<Transform>().rotation.w
        };

        //create a Player object at the location of the spawn point and with the prefab's rotation
        GameObject playerObject = Instantiate
        (
            PlayerPrefab,
            playerStartPos,
            new Quaternion
            (
                playerRotation[0],
                playerRotation[1],
                playerRotation[2],
                playerRotation[3]

            )
        );

        //create a list of the enemy start positions
        List<Vector3> enemyStartPositions = new List<Vector3>();

        //add the location of each enemy spawn in the editor to the list
        foreach (GameObject enemySpawn in EnemySpawns)
        {
            enemyStartPositions.Add(enemySpawn.GetComponent<Transform>().position);
        }

        //gets the number and type of enemy needed
        int enemyCount = PlayerPrefs.GetInt("Enemy#", 0);
        int enemyAlgoritm = PlayerPrefs.GetInt("EnemyALg", 0);

        //set the prefab to spawn based on the setting
        GameObject enemyPrefab = new GameObject();

        Enemy_Controller_A_Star enemyPrefabController = new Enemy_Controller_A_Star();
        if (enemyAlgoritm == 0)
        {
            enemyPrefab = EnemyAStarPrefab;

            enemyPrefabController = enemyPrefab.GetComponent<Enemy_Controller_A_Star>();
        }

        //tells the controller which map to use
        enemyPrefabController.Map = Floor.GetComponent<Tilemap>();

        //tells the controller which object is the player
        enemyPrefabController.Target = playerObject;

        //tells the controller which tilemap to show the path found on
        enemyPrefabController.pathfindIndicator = PathfindIndicator.GetComponent<Tilemap>();

        enemyPrefab.SetActive(false);

        //gets the rotation of the playerPrefab and stores it in an array
        float[] enemyRotation = new float[]
        {
            enemyPrefab.GetComponent<Transform>().rotation.x,
            enemyPrefab.GetComponent<Transform>().rotation.y,
            enemyPrefab.GetComponent<Transform>().rotation.z,
            enemyPrefab.GetComponent<Transform>().rotation.w
        };

        //create a list to hold the enemyObjects
        List<GameObject> enemyObjects = new List<GameObject>();

        //instatiate all the enemies required and save them to the list
        for (int i = 0; i <= enemyCount; i++)
        {
            GameObject temp = Instantiate
            (
                enemyPrefab,
                enemyStartPositions[i],
                new Quaternion
                (
                    enemyRotation[0],
                    enemyRotation[1],
                    enemyRotation[2],
                    enemyRotation[3]

                )
            );

            enemyObjects.Add(temp);
        }

        foreach (GameObject enemy in enemyObjects)
        {
            enemy.SetActive(true);
        }

        //set the player on the camera controller
        Camera.GetComponent<Camera_Controller>().Player = playerObject;
    }

}
