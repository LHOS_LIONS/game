﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_Controller : MonoBehaviour
{

    public List<MonoBehaviour> enemies;

    public MonoBehaviour enemyScript;

    //declare movement variables
    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float TurnSpeed = -3;

    private Rigidbody2D rb2d; //Used for the Rigidbody of the player

    // Start is called before the first frame update
    void Start()
    {
        // GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        // foreach (GameObject enemy in enemies)
        // {
        //     CircleCollider2D enemyCollider = enemy.GetComponent<CircleCollider2D>();
            
        //     enemyCollider.onTr
        // }

        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Get inputs
        float f = Input.GetAxis("Vertical");

        float r = Input.GetAxis("Horizontal");


        //Rotate the player by the desired amount
        rb2d.rotation = rb2d.rotation + r * TurnSpeed;

        //accelerate the player by the desired amount if possible
        if
        (
            f * rb2d.velocity.magnitude < maxSpeed //if the max speed has not already been reached
            && f != 0                              //and there is an input in the appropriate axis
        )
        {   
            Vector2 directionVector = calcDirectionVector(); //calculates the ratio of the vertical and horizontal components
            rb2d.AddForce(f * moveForce * directionVector);//multiplies this by the input in the vertical axis and applies it as a force
        }
    }

    
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            if (Global_Stats.time > PlayerPrefs.GetFloat("HighScore", 0.00f))
            {
                PlayerPrefs.SetFloat("HighScore", Global_Stats.time);
            }
            SceneManager.LoadScene("Game_Over");
        }
    }

    Vector2 calcDirectionVector()
    {
        float h = Mathf.Sin(Mathf.Deg2Rad * - rb2d.rotation); //calculate the horizontal value

        float v = Mathf.Cos(Mathf.Deg2Rad * rb2d.rotation); //calculate the vertical value

        return (new Vector2(h , v));
    }
}
