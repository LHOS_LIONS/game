﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class UI_Controller : MonoBehaviour
{
    float time;

    public Text timeText;
    public Text highText;
    public Button exit;


    void Start()
    {
        Global_Stats.time = 0;

        highText.text = "high score" + Math.Round(PlayerPrefs.GetFloat("HighScore") , 2).ToString();

        exit.onClick.AddListener(endGame);
    }
    //called on every frame

    void endGame()
    {
        if (Global_Stats.time > PlayerPrefs.GetFloat("HighScore", 0.00f))
            {
                PlayerPrefs.SetFloat("HighScore", Global_Stats.time);
            }
        SceneManager.LoadScene("Game_Over");
    }
    void Update()
    {
        Global_Stats.time += Time.deltaTime;
    
        time = Global_Stats.time;

        updateUI();
    }

    void updateUI()
    {
        timeText.text = "time: " + Math.Round(time, 2).ToString();
    }
}
